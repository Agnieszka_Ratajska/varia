import requests
import pandas as pd
import time
from bs4 import BeautifulSoup

def generate_urls(base_url, num_pages):
    return [f"{base_url}{i}.html" for i in range(1, num_pages + 1)]


def scrape_data_to_csv(base_url, num_pages, output_file):
    urls = generate_urls(base_url, num_pages)
    
    pgn = []
    
    for url in urls:
        headers = {"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36"}
        response = requests.get(url, headers=headers)
        soup = BeautifulSoup(response.content, "html.parser")
        
        text_1 = soup.find("h1", class_="").get_text(strip=True)
        text_2 = soup.find("h2", class_="").get_text(strip=True)
        
        pgn.append({
            "text_1": text_1,
            "text_2": text_2,
            "link": url
        })
    
    df = pd.DataFrame(pgn)
    df.to_csv(output_file, index=False)
    print("Done")

base_url = "http://..."
num_pages = 10
output_file = "file_10.csv"

scrape_data_to_csv(base_url, num_pages, output_file)