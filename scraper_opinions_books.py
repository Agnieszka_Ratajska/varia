import requests
from bs4 import BeautifulSoup
import csv
import time
import random


class OpinionScraper:
    def __init__(self, urls, start_page=1, end_page=500, output_file="opinions.csv"):
        self.urls = urls
        self.start_page = start_page
        self.end_page = end_page
        self.output_file = output_file


    def scrape(self):
        with open(self.output_file, "w", newline="", encoding="utf-8") as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(["text", "url"]) 

            for url_base, url_end in self.urls:
                for page in range(self.start_page, self.end_page + 1):
                    current_url = f"{url_base}{page}{url_end}"
                    headers = {"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36"}
                    print({current_url})

                    success = False
                    for attempt in range(5):  # We try up to 5 times
                        try:
                            response = requests.get(current_url, headers=headers)
                            response.raise_for_status()
                            content = response.text
                            soup = BeautifulSoup(content, "html.parser")

                            # All opinions on the page
                            opinion_elements = soup.find_all("p", {"class": "expandTextNoJS p-expanded js-expanded mb-0"})# To adapt to data from the page code
                        
                            for element in opinion_elements:
                                opinion = element.text.strip()
                                writer.writerow([opinion, current_url])
                            success = True
                            break 
                        except requests.exceptions.RequestException as e:
                            print(f"Error {current_url}, attemp {attempt+1}, except: {e}")
                            time.sleep(self.random_delay())   

                    if not success:
                        with open("error_log.txt", "a", encoding="utf-8") as error_log:
                            error_log.write(f"Error for page: {current_url}\n")

                    time.sleep(self.random_delay())

        print("Finished")

    def random_delay(self):
        return round(random.uniform(1, 3), 2)

# Class usage
#urls = [
#    ("https://page...", "&listId=listId_72085_4&sortBy=youngest&rating%5B0%5D=10&rating%5B1%5D=10&showFirstLetter=0&paginatorType=Standard&paginatorType=Standard"),
#    ("https://page...", "&listId=listId_72085_4&sortBy=youngest&rating%5B0%5D=1&rating%5B1%5D=1&showFirstLetter=0&paginatorType=Standard&paginatorType=Standard"),
#    ("https://page...", "&listId=listId_72085_4&sortBy=youngest&rating%5B0%5D=5&rating%5B1%5D=5&showFirstLetter=0&paginatorType=Standard&paginatorType=Standard")
#]

#scraper = OpinionScraper(urls, start_page=1, end_page=5)
#scraper.scrape()